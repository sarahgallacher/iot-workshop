# IoT Workshop

Step-by-step instructions for the IoT workshop activities

---

**All content for this workshop has already been installed on the Pi Zeros. However if you are starting from scratch with a new Pi Zero follow these instructions to install required libraries and code samples:**

## To Install
1. Clone this directory by running the `git clone` command
2. Navigate into the cloned directory and run `sh install.sh`

## Activities
- Activity 1: GPIOs, sensing and actuating on a RPi Zero
- Activity 2: Pushing data from an RPi Zero to the cloud
- Activity 3: Requesting data from a web API onto an RPi Zero