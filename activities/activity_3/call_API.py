from requests import get
import json
from pprint import pprint
from haversine import haversine

#*********************
# Global variables
#*********************
# URLS to call to retrieve data
stations = 'https://apex.oracle.com/pls/apex/raspberrypi/weatherstation/getallstations'
weather = 'https://apex.oracle.com/pls/apex/raspberrypi/weatherstation/getlatestmeasurements/'

# Location values - lat, lon
my_location = (52.194504, 0.134708)


#*********************
# Function
#*********************
def find_closest():
    smallest = 20036 # Max distance between 2 points on the earth's surface

    # first get all weather stations
    print("getting all weather stations...")
    all_stations = get(stations).json()['items']

    # now iterate through all stations and check their distance from our current position
    print("checking for closest station...")
    for station in all_stations:
    	# Get location of the current station
    	station_location = (station['weather_stn_long'], station['weather_stn_lat'])

    	# Check the distance between current station and our location
    	distance = haversine(my_location, station_location)

    	# if distance between current station and our current location is smallest so far
    	# then set this distance as the smallest and set the weather station ID as the closest
    	if distance < smallest:
        	smallest = distance
        	closest_station = station['weather_stn_id']
	
	# Once we have iterated through all stations and checked their distances
	# return the closest station
	return closest_station




#********************************
# Code starts to run from here
#********************************
# Get the closest station to our current location
closest_stn = find_closest()

# Add closest station ID to weather request URL
weather = weather + str(closest_stn)

# call weather API and print weather from the closest station
print("getting local weather from closest station...")
my_weather = get(weather).json()['items'][0]
pprint(my_weather)
