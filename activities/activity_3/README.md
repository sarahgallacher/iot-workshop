# IoT Workshop

Step-by-step instructions for the IoT workshop activities

---

## Activity 3: Requesting data from a web API onto an RPi Zero

In this activity we will request weather data from an API on the internet, and use the returned weather data locally on the RPi. The API that we are using gives access to a network of weather stations all over the world. We can find our closest weather station and get the latest weather data from it.

### Requesting weather data
* Navigate to the folder for activity 3 by typing: `cd ~/iot-workshop/activities/activity_3`
* Open the Nano text editor to view the example code by typing: `nano call_API.py` (you can also view the code in your browser by clicking on source (left menu) -> activities -> activity_3 -> call_API.py)
* Explore the code reading the comments to understand what it is doing - can you identify the part where you specify the latitude (lat) and longitude (lon) of your location?
* The `my_location` variable specifies the lat, lon of your location - it is currently set to the location of Buckingham Palace. Let's change it to be more accurate
    * Open a browser window on your laptop and go to `maps.google.com`
    * Search for `3 St Paul's Place Sheffield` and zoom in on the map to our current location 
    * Click on the map at our location to create a grey pin as shown below
    * At the bottom of the map you should see a small popup with the address and 2 decimal numbers underneath (see example image below) - the first is the lat and the second is the lon
    * Copy the lat and lon from google maps into your code updating the values that are currently in `my_location`
    
![Scheme](../../images/map.png)
    
* Now save you changes in the nano text editor by typing: `CTRL + o`, then hit ` return` to confirm saving
* Close the nano text editor by typing: `CTRL + x`
* When you run this python script it should make a call to the weather API and get data from your closest weather station based on your updated lat, lon
* The API will return a result like this which will be printed on your screen giving details of the latest weather data from your nearest weather station:

{u'air_pressure': 1006.51,  
 u'air_quality': 48.08,  
 u'ambient_temp': 20.26,  
 u'created_by': u'Pi Towers Demo',  
 u'created_on': u'2018-12-18T11:55:04Z',  
 u'ground_temp': 15.94,  
 u'humidity': 37.76,  
 u'id': 14690398,  
 u'rainfall': 0,  
 u'reading_timestamp': u'2018-12-18T11:55:04Z',  
 u'updated_by': u'Pi Towers Demo',  
 u'updated_on': u'2018-12-18T12:00:15.536Z',  
 u'weather_stn_id': 255541,  
 u'wind_direction': 180,  
 u'wind_gust_speed': 0,  
 u'wind_speed': 0}  
 
 * Run the script to see what happens by typing: `python call_API.py`
 
### Controlling an LED based on weather data
* **Make sure you have connected up the LED as shown in activity 1** 
* Navigate to the folder for activity 3 by typing: `cd ~/iot-workshop/activities/activity_3`
* Open the Nano text editor to view the example code by typing: `nano weather_LED.py` (you can also view the code in your browser by clicking on source (left menu) -> activities -> activity_3 -> weather_LED.py) 
* Explore the code reading the comments to understand what it is doing, it will look very similar to the previous example but with a few extra lines added - can you identify the part where the LED is turned on or off depending on the ambient_temp 
* Close the nano text editor by typing: `CTRL + x`
* Run the python script by typing: `python weather_LED.py` 
* If the ambient_temp is greater than 20 the LED will turn on - check the ambient_temp on the printout and check the LED is correctly on or off 
* Now try to change the code to either: 
    * turn the LED on when the humidity is below 40 
    * make the LED blink when ambient_temp is greater than 20
    
#### Reminder of how to edit files in Nano
* Open Nano text editor: `nano weather_LED.py` and make changes to your code
* To save your changes type: `CTRL + o`, then hit return to confirm saving
* To close Nano type `CTRL + x`
* To run code type `python weather_LED.py`
 
### Explore!
* Can you change the code to get the latest weather from the Mount Everest or the Taj Mahal?
* Can you change the code so that the LED blinks when the humidity is above 40?
* Can you add an extra LED so the red LED comes on if air quality is less than 50 and the green LED comes on if the air quality is 50 or more?