# IoT Workshop

Step-by-step instructions for the IoT workshop activities

---

## Activity 2: Pushing data from an RPi Zero to the cloud
In this activity we will read data from a sensor attached to the RPi and then send the data to the cloud so that we can see it visualised on a Grafana dashboard.

### Sending data to the cloud
* **Make sure you have the light sensor hooked up as shown in activity 1**
* Navigate to the folder for activity 2 by typing: `cd ~/iot-workshop/activities/activity_2`
* Open the Nano text editor to view the example code by typing: `nano send_to_cloud.py` (you can also view the code in your browser by clicking on source (left menu) -> activities -> activity_2 -> send_to_cloud.py)
* Explore the code reading the comments to understand what it is doing - can you identify the part where you specify the AWS topic that your data will be sent to?
* Make sure that the topic ends with your team name, so e.g. if your team name is `luigi` it should look like: `iotworkshop/luigi`
* When you run the script it will take a reading from the light sensor, get the current time and date, and send all of this to AWS as a JSON object like:  

{  
    "dev_id": "luigi",      
	"timestamp": "2019-03-05T18:46:24",  
	"value": 0.589387925235  
}
  
* To run the script type: `python send_to_cloud.py`
* You should see a successful read and upload message printed on the screen every 10 seconds
* **Let the script continue to run so you have data to visualise in the next steps**

### Visualising sensor data in Grafana
* **Make sure you are connected to the nyquist-corp-legacy network**
* Open a browser on your laptop and in the address bar type: `http://34.242.191.230:3000`
* At the login screen enter your credentials:
    * username: `your team name` (e.g. dumbo)
    * password: `your team name + Pa55` (e.g. dumboPa55)
* You should now see a dark screen with your team name at the top (see image below) - this is your team dashboard and where you will create your data visualisation

![Scheme](../../images/grafana_1.JPG)

* Follow the instructions on the screenshots below to create your visualisation

**1. Create a new Graph panel on your dashboard**

![Scheme](../../images/grafana_2.png)
![Scheme](../../images/grafana_3.png)

**2. Set the dashboard time scale and update rate **

![Scheme](../../images/grafana_4.png)

**3. Configure the data settings**

![Scheme](../../images/grafana_5.png)

**4. Configure the display settings**

![Scheme](../../images/grafana_6.png)

**5. Configure the graph axes**

![Scheme](../../images/grafana_7.png)

**6. Configure the general settings including the graph title, save and return to dashboard**

![Scheme](../../images/grafana_8.png)

**7. Done!**
* This dashboard doesn't show any data but hopefully yours does

![Scheme](../../images/grafana_9.JPG)

## Explore!
* Congrats! You have created a Grafana visualisation for your dashboard
* Try adding additional data lines (metrics) to your graph to show data from other teams' sensors (clue - to do this click the dropdown beside your panel name, then click the data settings icon on the left, then click `Add Query` below your first query)
* Try creating another visualisation panel for your dashboard using one of the other visualisations that Grafana provides

