# script to connect light sensor to AWS IoT
# author: SG / JG


# Modules required to run script
# Import modules to interface with AWS cloud service programatically
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import datetime
import json

# Import modules requored to interface with light sensor
from gpiozero import LightSensor
from time import sleep


# Global variables
# AWS interfacing variables
aws_topic = 'iotworkshop/luigi'  # AWS topic
myMQTTClient = AWSIoTMQTTClient("luigi")  # AWS Thing ID

# Light sensor variables
sensor = LightSensor(18) # light sensor pin
sample_rate = 10 # set how often light sensor is read in seconds


# AWS settings
# Thing Rest API Endpoint
myMQTTClient.configureEndpoint("a39px5nj52fh6x-ats.iot.eu-west-1.amazonaws.com", 8883)

# Login credentials
myMQTTClient.configureCredentials(
    "/home/pi/credentials/root_ca.pem", 
    "/home/pi/credentials/e57c195fa4-private.pem.key", 
    "/home/pi/credentials/e57c195fa4-certificate.pem.crt")

# Client connection configuration
myMQTTClient.configureOfflinePublishQueueing(-1) # Infinite offline Publish queueing
myMQTTClient.configureDrainingFrequency(2) # Draining: 2Hz
myMQTTClient.configureConnectDisconnectTimeout(10) # 10 sec
myMQTTClient.configureMQTTOperationTimeout(5) # 5 sec


# Function for sending data to AWS
def sendToAWSIoT(data): 
	# Connect and publish data on reading and spectra
	try:
		myMQTTClient.connect()
		if myMQTTClient.publish(aws_topic, data, 0) == True:
			myMQTTClient.disconnect()
			return True
	except:
		print("Could not connect to cloud - data not uploaded")
	return False


#********************************
# Code starts running from here
#********************************
print("Ready\n\n")

while True:
    # read sensor value and timestamp
    timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    value = sensor.value
    print("timestamp: " + str(timestamp) + ", sensor value: " + str(value))

    # send timestamp and sensor reading to cloud
    print("sending reading to cloud...")
    reading = {
        "dev_id": "luigi",
    	"timestamp": timestamp,
    	"value": value
    }
    sendToAWSIoT(json.dumps(reading))
    print("sent!")
    sleep(sample_rate)
