from gpiozero import Button
from signal import pause

button = Button(2) # select correct pin number


def pressed():
    print("Button pressed")


def released():
    print("Button released")


print("Ready\n\n")

button.when_pressed = pressed
button.when_released = released

pause()
