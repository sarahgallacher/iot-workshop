from gpiozero import LED
from time import sleep

led = LED(17) # choose the correct pin number
blink_rate = 1 # in seconds


print("Ready\n\n")

while True:
    print("Turning LED on")
    led.on()
    sleep(blink_rate)
    print("Turning LED off")
    led.off()
    sleep(blink_rate)
