from gpiozero import LightSensor
from time import sleep

sensor = LightSensor(18) # light sensor pin
sample_rate = 1 # set how often light sensor is read


print("Ready\n\n")

while True:
    print("sensor value: " + str(sensor.value))
    sleep(sample_rate)
