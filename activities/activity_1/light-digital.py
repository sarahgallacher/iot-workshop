from gpiozero import LightSensor

sensor = LightSensor(18)


print("Ready\n\n")

while True:
    sensor.wait_for_light()
    print("It's light! :)")
    sensor.wait_for_dark()
    print("It's dark :(")
